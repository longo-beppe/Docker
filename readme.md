# Apache with PHP 5.6|7.0|7.1 on Ubuntu 16.04 LTS (Xenial Xerus)

This images provides a common PHP hosting environment. The intent is for the PHP application itself to be stored in persistent storage wihch is then mounted in to this image at `/var/www`

- https://hub.docker.com/r/1and1internet/ubuntu-16-apache-php-5.6/
- https://hub.docker.com/r/1and1internet/ubuntu-16-apache-php-7.0/
- https://hub.docker.com/r/1and1internet/ubuntu-16-apache-php-7.1/

```
web_php_56:
  build: ./image/ubuntu-16-apache-php-5.6
```
```
web_php_70:
  build: ./image/ubuntu-16-apache-php-7.0
```
```
web_php_70:
  build: ./image/ubuntu-16-apache-php-7.1
```


To change UID e GUID edit Dockerfile:

```
useradd -u  1000 -g 100 dckuser && \
usermod -s /bin/bash dckuser && \
echo "export APACHE_RUN_USER=dckuser" >> /etc/apache2/envvars && \
echo "export APACHE_RUN_GROUP=users" >> /etc/apache2/envvars && \
```

Volumes are:
  - `progect path`:/var/www/html
  - `mysqld.sock path`mysqld.sock:/var/run/mysqld/mysqld.sock
  - `local xdebug.ini`:/etc/php/`7.0`/apache2/conf.d/20-xdebug.ini
